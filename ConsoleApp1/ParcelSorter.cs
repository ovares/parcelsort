﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class ParcelSorter
    {
        private readonly int boxLength;
        private readonly int boxWidth;
        private readonly int[] tubeWidths;

        public ParcelSorter(int boxX, int boxY, int[] tubeW)
        {
            boxLength = Math.Max(boxX, boxY);
            boxWidth = Math.Min(boxX, boxY);
            tubeWidths = tubeW;
        }

        public bool Calculate()
        {
            for (int i = 1; i < tubeWidths.Count(); i++)
            {
                var fit = Compare(tubeWidths[i - 1], tubeWidths[i]);
                if (!fit)
                {
                    return false;
                }
            }
            return true;
        }

        private bool Compare(int tubeX, int tubeY)
        {
            var calc = Math.Sqrt(tubeX * tubeX + tubeY * tubeY) - boxLength / 2;
            return (calc >= boxWidth) ? true : false;
        }

    }
}
