﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] InputA = { 60, 120, 100, 75 };
            int[] InputB = { 100, 35, 75, 50, 80, 100, 37 };
            int[] InputC = { 70, 50, 60, 60, 55, 90 };

            // Example A
            if (InputA.Count() < 4)
            {
                Console.WriteLine("Invalid input - too few parameters");
            }
            var parcelSorter1 = new ParcelSorter(InputA[0], InputA[1], InputA.SubArray(2, InputA.Count()-2));
            var doesFit1 = parcelSorter1.Calculate();
            if (doesFit1)
            {
                 Console.WriteLine("Fits");
            }
            else
            {
                Console.WriteLine("Does not fit");
            }

            // Example B
            if (InputB.Count() < 4)
            {
                Console.WriteLine("Invalid input - too few parameters");
            }
            var parcelSorter2 = new ParcelSorter(InputB[0], InputB[1], InputB.SubArray(2, InputB.Count() - 2));
            var doesFit2 = parcelSorter2.Calculate();
            if (doesFit2)
            {
                Console.WriteLine("Fits");
            }
            else
            {
                Console.WriteLine("Does not fit");
            }

            // Example C
            if (InputC.Count() < 4)
            {
                Console.WriteLine("Invalid input - too few parameters");
            }
            var parcelSorter3 = new ParcelSorter(InputC[0], InputC[1], InputC.SubArray(2, InputC.Count() - 2));
            var doesFit3 = parcelSorter3.Calculate();
            if (doesFit3)
            {
                Console.WriteLine("Fits");
            }
            else
            {
                Console.WriteLine("Does not fit");
            }

        }
    }
}
