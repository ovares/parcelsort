# Parcel sorting line 
Parcel sorting center has a pipe that passes parcels from one qualification point to another.  Parcels are rectangles with predefined measurements and the pipe has predefined width.  The pipe is not a straight line and makes several 90-degree turns, pipe width changes with each turn. 
Write a program that returns info whether the parcel can fit through the pipe or not. Please consider the pipe and parcels as two-dimensional objects.  The length of the pipe is sufficient and should not be taken into account.  The problem lies in the corners, can the parcel fit through them? 
The program input has comma-separated integers,  first two are the dimensions for the parcel and the rest describe pipe widths.  The widths are given in the direction of the parcel movement. 
 
