﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ConsoleApp1;
using System.Linq;

namespace ParcelSort.Test
{
    [TestClass]
    public class ParcelTests
    {
        [TestMethod]
        public void TestWithValidInput1()
        {
            int[] P = { 60, 120, 100, 75 };

            var parcelSorter = new ParcelSorter(P[0], P[1], P.SubArray(2, P.Count() - 2));
            var doesFit = parcelSorter.Calculate();

            Assert.AreEqual(doesFit, true);
        }

        [TestMethod]
        public void TestWithValidInput2()
        {
            int[] P = { 100, 35, 75, 50, 80, 100, 37 };

            var parcelSorter = new ParcelSorter(P[0], P[1], P.SubArray(2, P.Count() - 2));
            var doesFit = parcelSorter.Calculate();

            Assert.AreEqual(doesFit, true);
        }

        [TestMethod]
        public void TestWithInvalidInput()
        {
            int[] P = { 70, 50, 60, 60, 55, 90 };

            var parcelSorter = new ParcelSorter(P[0], P[1], P.SubArray(2, P.Count() - 2));
            var doesFit = parcelSorter.Calculate();

            Assert.AreEqual(doesFit, false);
        }
    }
}
